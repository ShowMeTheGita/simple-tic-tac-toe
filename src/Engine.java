public class Engine {

    private Board board;
    private int cellsize;
    private ClickListener mouse;
    private Xs xs;
    private CPUVersus cpuVersus;

    private boolean cpuHasPlayed;
    private boolean playerHasPlayed;

    public Engine(int cellsize) {

        board = new Board (cellsize);
        xs = new Xs(board);
        mouse = new ClickListener();

        mouse.init(board);

        cpuVersus = new CPUVersus(board, xs);
        cpuVersus.setClickListener(mouse);
        mouse.setCpuVersus(cpuVersus);


        cpuHasPlayed = cpuVersus.getCpuHasPlayed();

        engine();

    }

    private void engine() {

        for (int i = 0; i < cpuVersus.getHasBeenDrawn().length; i++) {

            while (!cpuVersus.getHasBeenDrawn()[i]) {
                cpuVersus.cpuMove();
            }
        }

    }








}
