import org.academiadecodigo.simplegraphics.graphics.Line;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Xs {

    private Line lineOne;
    private Line lineTwo;

    private double lineOneStartingX;
    private double lineOneStartingY;
    private double lineOneEndingX;
    private double lineOneEndingY;

    private Board board;
    double dynamicXSizeReduction = Board.PADDING_X;


    public Xs(Board board) {

        this.board = board;

        //lineOne = new Line (lineOneStartingX, lineOneStartingY, lineOneEndingX, lineOneEndingY);
        //lineTwo = new Line(lineOneEndingY, lineOneStartingX, lineOneStartingY, lineOneEndingX);


    }


    public void configureFirstLinePositions(Rectangle square, int i) {

        lineOneStartingX = square.getX() + dynamicXSizeReduction;
        lineOneStartingY = square.getY() + dynamicXSizeReduction;
        lineOneEndingX   = square.getWidth() * (i + 1) - dynamicXSizeReduction;
        lineOneEndingY   = square.getHeight()   - dynamicXSizeReduction;

        lineOne = new Line(lineOneStartingX, lineOneStartingY, lineOneEndingX, lineOneEndingY);
        lineTwo = new Line(lineOneEndingY + square.getHeight()*i, lineOneStartingX - (square.getHeight()*i), lineOneStartingY + square.getHeight() * i, lineOneEndingX - (square.getHeight()* i));

    }

    public void configureSecondLinePositions(Rectangle square, int i) {

        lineOneStartingX = square.getX()  + dynamicXSizeReduction;
        lineOneStartingY = square.getY()  + dynamicXSizeReduction;
        lineOneEndingX   = square.getWidth() * (i + 1) - dynamicXSizeReduction;
        lineOneEndingY   = square.getHeight() + (i + 1)   - dynamicXSizeReduction;

        lineOne = new Line(lineOneStartingX , lineOneStartingY, lineOneEndingX, lineOneEndingY + square.getHeight());
        lineTwo = new Line(lineOneEndingY + square.getHeight()*i , lineOneStartingX - (square.getHeight()*i) + square.getHeight() , lineOneStartingY + square.getHeight() * i - square.getHeight(), lineOneEndingX - square.getHeight() * i + square.getHeight());

    }

    public void configuredThirdLinePositions(Rectangle square, int i) {

        lineOneStartingX = square.getX()  + dynamicXSizeReduction;
        lineOneStartingY = square.getY()  + dynamicXSizeReduction;
        lineOneEndingX   = square.getWidth() * (i + 1) - dynamicXSizeReduction;
        lineOneEndingY   = square.getHeight() + (i + 1)   - dynamicXSizeReduction;

        lineOne = new Line(lineOneStartingX , lineOneStartingY, lineOneEndingX, lineOneEndingY + square.getHeight() + square.getHeight());
        lineTwo = new Line(lineOneEndingY + square.getHeight()*i , lineOneStartingX - (square.getHeight()*i) + square.getHeight() + square.getHeight(), lineOneStartingY + square.getHeight() * i - square.getHeight() - square.getHeight(), lineOneEndingX - square.getHeight() * i + square.getHeight() + square.getHeight());

    }




    public void drawX() {

        lineOne.draw();
        lineTwo.draw();

    }


}
