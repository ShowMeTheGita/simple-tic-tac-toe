import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Ellipse;
import org.academiadecodigo.simplegraphics.graphics.Line;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import javax.swing.*;

public class Board {

    private Rectangle[][] board;
    private Rectangle[][] centerPoints;
    private Ellipse[][] ellipses;
    private int cellsize;
    private static final int ROWS_AND_COLUMNS = 3;
    public static final int PADDING = 10;
    public static final double PADDING_X = PADDING*3;
    private Line line;


    public Board (int cellsize) {


        this.cellsize = cellsize;

        init();
        whiteLineBoarders();


    }


    public void init() {

        board = new Rectangle[ROWS_AND_COLUMNS][ROWS_AND_COLUMNS];
        centerPoints = new Rectangle[ROWS_AND_COLUMNS][ROWS_AND_COLUMNS];
        ellipses = new Ellipse[ROWS_AND_COLUMNS][ROWS_AND_COLUMNS];


        for (int i = 0; i < ROWS_AND_COLUMNS; i++) {

            for (int j = 0; j < ROWS_AND_COLUMNS; j++) {

                Rectangle oneSquare;
                Rectangle oneSquareCenters;
                Ellipse oneEllipse;

                board[i][j] = oneSquare = new Rectangle(j * cellsize + PADDING, i * cellsize + PADDING, cellsize, cellsize);
                centerPoints[i][j] = oneSquareCenters = new Rectangle((j * (cellsize)) + (cellsize/2), (i * (cellsize) ) + (cellsize/2), 50, 50);
                ellipses[i][j] = oneEllipse = new Ellipse(j * cellsize + PADDING + 25 , i * cellsize + 25 + PADDING, 50, 50);

                oneSquare.draw();
                //oneSquareCenters.draw();
                //oneEllipse.draw();


            }
        }
    }


    public void whiteLineBoarders() {

        Rectangle topBoarder = new Rectangle(PADDING, PADDING, cellsize * ROWS_AND_COLUMNS, 0 );
        Rectangle bottomBoarder = new Rectangle(PADDING, cellsize * ROWS_AND_COLUMNS + PADDING, cellsize * ROWS_AND_COLUMNS, 0);
        Rectangle leftBoarder = new Rectangle(PADDING, PADDING, 0, cellsize * ROWS_AND_COLUMNS);
        Rectangle rightBoarder = new Rectangle(cellsize * ROWS_AND_COLUMNS + PADDING, PADDING, 0, cellsize * ROWS_AND_COLUMNS);


        topBoarder.setColor(Color.WHITE);
        topBoarder.draw();

        bottomBoarder.setColor(Color.WHITE);
        bottomBoarder.draw();

        leftBoarder.setColor(Color.WHITE);
        leftBoarder.draw();

        rightBoarder.setColor(Color.WHITE);
        rightBoarder.draw();

    }

    public Rectangle[][] getCenterPoints() {

        return centerPoints;
    }

    public Rectangle[][] getBoardSquares() {

        return board;
    }

    public Rectangle getSpecificRectangle(int i, int j) {

        return board[i][j];

    }

    public int getCellsize() {
        return cellsize;

    }




}
