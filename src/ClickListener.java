import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.mouse.Mouse;
import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.simplegraphics.mouse.MouseEventType;
import org.academiadecodigo.simplegraphics.mouse.MouseHandler;

public class ClickListener implements MouseHandler {

    private Board board;
    private Mouse mouse;
    private Rectangle[][] squares;
    private Xs xs;
    private Os os;
    private CPUVersus cpuVersus;


    public void init(Board board) {

        mouse = new Mouse(this);
        mouse.addEventListener(MouseEventType.MOUSE_CLICKED);

        this.board = board;
        squares = board.getBoardSquares();

        os = new Os(board);
        xs = new Xs(board);



    }



    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        System.out.println("x: " + mouseEvent.getX() + " y: " + mouseEvent.getY());

        clickingInsideSquare00(mouseEvent);
        clickingInsideSquare01(mouseEvent);
        clickingInsideSquare02(mouseEvent);
        clickingInsideSquare10(mouseEvent);
        clickingInsideSquare11(mouseEvent);
        clickingInsideSquare12(mouseEvent);
        clickingInsideSquare20(mouseEvent);
        clickingInsideSquare21(mouseEvent);
        clickingInsideSquare22(mouseEvent);

    }


    private void clickingInsideSquare00(MouseEvent mouseEvent) {


        if (mouseEvent.getX() < squares[0][0].getWidth() + Board.PADDING && mouseEvent.getY() < squares[0][0].getHeight() + 50) {

            System.out.println("clicking 00");

           if (!cpuVersus.getHasBeenDrawn()[0]) {
               os.configureFirstLinePositions(1, 0);
               os.drawO();
               cpuVersus.setHasBeenDrawn(true, 0);
               cpuVersus.setWhoHasDrawn(4, 0);
               cpuVersus.setCpuPlayed(false);
           } else {
              invalidMove();
           }

        }

    }


    private void clickingInsideSquare01(MouseEvent mouseEvent) {

        if (mouseEvent.getX() > (squares[0][1].getX()) && mouseEvent.getX() < squares[0][2].getX() && mouseEvent.getY() < squares[1][0].getY() - Board.PADDING + 50) {

            System.out.println("clicking 01");


            if (!cpuVersus.getHasBeenDrawn()[1]) {
                os.configureFirstLinePositions(1, 1);
                os.drawO();
                cpuVersus.setHasBeenDrawn(true, 1);
                cpuVersus.setWhoHasDrawn(4, 1);
                cpuVersus.setCpuPlayed(false);
            } else {
               invalidMove();
            }


        }

    }


    private void clickingInsideSquare02(MouseEvent mouseEvent) {

        if (mouseEvent.getX() > squares[0][2].getX() && mouseEvent.getY() < squares[1][2].getY() - Board.PADDING + 50) {

            System.out.println("clicking 02");


            if (!cpuVersus.getHasBeenDrawn()[2]) {
                os.configureFirstLinePositions(1, 2);
                os.drawO();
                cpuVersus.setHasBeenDrawn(true, 2);
                cpuVersus.setWhoHasDrawn(4,2);
                cpuVersus.setCpuPlayed(false);
            } else {
                invalidMove();
            }


        }

    }


    private void clickingInsideSquare10(MouseEvent mouseEvent) {

        if (mouseEvent.getX() < squares[0][1].getX() && (mouseEvent.getY() < squares[2][0].getY() + 50 - Board.PADDING) && (mouseEvent.getY() > squares[1][0].getY()  + 50 - Board.PADDING)) {

            System.out.println("clicking 10");

            if (!cpuVersus.getHasBeenDrawn()[3]) {
                os.configureFirstLinePositions(2, 0);
                os.drawO();
                cpuVersus.setHasBeenDrawn(true, 3);
                cpuVersus.setWhoHasDrawn(4,3);
                cpuVersus.setCpuPlayed(false);
            } else {
                invalidMove();
            }


        }

    }


    private void clickingInsideSquare11(MouseEvent mouseEvent) {

        if (mouseEvent.getX() < squares[1][2].getX() && mouseEvent.getX() > squares[1][1].getX() && mouseEvent.getY() <  squares[2][0].getY() + 50 - Board.PADDING && mouseEvent.getY() > squares[1][0].getY()  + 50 - Board.PADDING) {

            System.out.println("clicking 11");

            if (!cpuVersus.getHasBeenDrawn()[4]) {
                os.configureFirstLinePositions(2, 1);
                os.drawO();
                cpuVersus.setHasBeenDrawn(true, 4);
                cpuVersus.setWhoHasDrawn(4,4);
                cpuVersus.setCpuPlayed(false);
            } else {
                invalidMove();
            }


        }

    }


    private void clickingInsideSquare12(MouseEvent mouseEvent) {

        if (mouseEvent.getX() > squares[1][2].getX() && mouseEvent.getY() > squares[1][1].getX() && mouseEvent.getY() < squares[2][0].getY() + 50 - Board.PADDING && mouseEvent.getY() > squares[1][0].getY() + 50 - Board.PADDING) {

            System.out.println("clicking 12");

            if (!cpuVersus.getHasBeenDrawn()[5]) {
                os.configureFirstLinePositions(2, 2);
                os.drawO();
                cpuVersus.setHasBeenDrawn(true, 5);
                cpuVersus.setWhoHasDrawn(4, 5);
                cpuVersus.setCpuPlayed(false);
            } else {
                invalidMove();
            }

        }

    }


    private void clickingInsideSquare20(MouseEvent mouseEvent) {

        if (mouseEvent.getX() < squares[2][1].getX() && mouseEvent.getY() > squares[2][0].getY() + 50 - Board.PADDING) {

            System.out.println("clicking 20");

            if (!cpuVersus.getHasBeenDrawn()[6]) {
                os.configureFirstLinePositions(3, 0);
                os.drawO();
                cpuVersus.setHasBeenDrawn(true, 6);
                cpuVersus.setWhoHasDrawn(4, 6);
                cpuVersus.setCpuPlayed(false);
            } else {
                invalidMove();
            }

        }

    }


    private void clickingInsideSquare21(MouseEvent mouseEvent) {

        if (mouseEvent.getX() < squares[2][2].getX() && mouseEvent.getX() > squares[2][1].getX() && mouseEvent.getY() > squares[2][1].getY() + 50 - Board.PADDING) {

            System.out.println("clicking 21");

            if (!cpuVersus.getHasBeenDrawn()[7]) {
                os.configureFirstLinePositions(3, 1);
                os.drawO();
                cpuVersus.setHasBeenDrawn(true, 7);
                cpuVersus.setWhoHasDrawn(4, 7);
                cpuVersus.setCpuPlayed(false);
            } else {
                invalidMove();
            }

        }

    }


    private void clickingInsideSquare22(MouseEvent mouseEvent) {

        if (mouseEvent.getX() > squares[2][2].getX() && mouseEvent.getY() > squares[2][2].getY() + 50 - Board.PADDING) {

            System.out.println("clicking 22");

            if (!cpuVersus.getHasBeenDrawn()[8]) {
                os.configureFirstLinePositions(3, 2);
                os.drawO();
                cpuVersus.setHasBeenDrawn(true, 8);
                cpuVersus.setWhoHasDrawn(4, 8);
                cpuVersus.setCpuPlayed(false);
            } else {
                invalidMove();
            }

        }

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

        System.out.println("move");

    }

    public void setCpuVersus(CPUVersus cpuVersus) {
        this.cpuVersus = cpuVersus;
    }

    private void invalidMove() {
        System.out.println("Invalid move, CPU has played here.");
    }


}


