package StaticVsNonStatic;

public class Playground {
    public static void main(String[] args) {

        StaticProperty s1 = new StaticProperty(5);
        StaticProperty s2 = new StaticProperty(6);
        StaticProperty s3 = new StaticProperty(7);

        NonStaticProperty ns1 = new NonStaticProperty();
        NonStaticProperty ns2 = new NonStaticProperty();

        System.out.println(s1.getValue() + " " + s2.getValue() + " " + s3.getValue());
        System.out.println(ns1.getValue() + " " + ns2.getValue());

        StaticProperty.setValue(50);
        System.out.println(s1.getValue() + " " + s2.getValue() + " " + s3.getValue());

        ns2.setValue(40);
        System.out.println(ns1.getValue());


    }
}
