package StaticVsNonStatic;

public class StaticProperty {

    private static int value;


    public StaticProperty(int value) {

        StaticProperty.value = value;

    }


    public int getValue() {
        return value;
    }

    public static void setValue(int number) {
        value = number;
    }


}
