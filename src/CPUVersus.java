import org.academiadecodigo.simplegraphics.mouse.MouseEvent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class CPUVersus {

    private Xs xs;
    private Board board;
    private ClickListener clickListener;
    private MouseEvent mouseEvent;


    private int[] whoHasDrawn;

    private int[] firstHorizontalLine;
    private int[] secondHorizontalLine;
    private int[] thirdHorizontalLine;

    private int[] firstVerticalLine;
    private int[] secondVerticalLine;
    private int[] thirdVerticalLine;

    private int[] diagonalLtoR;
    private int[] diagonalRtoL;


    private boolean[] hasBeenDrawn;

    private boolean[] firsHorizontalLineDrawn;
    private boolean[] secondHorizontalLineDrawn;
    private boolean[] thirdHorizontalLineDrawn;

    private boolean[] firstVerticalLineDrawn;
    private boolean[] secondVerticalLineDrawn;
    private boolean[] thirdVerticalLineDrawn;

    private boolean[] lToRDiagonalDrawn;
    private boolean[] rToLDiagonalDrawn;


    private boolean[] lossAverted;
    private boolean willSkipATurn;
    private boolean cpuPlayed;
    private boolean aboutToLoseVertically;
    private boolean abouToLoseHorizontally;
    private boolean aboutToLoseDiagonally;


    public CPUVersus(Board board, Xs xs) {

        this.board = board;
        this.xs = xs;

        cpuPlayed = false;


        hasBeenDrawn = new boolean[9];
        Arrays.fill(hasBeenDrawn, false);

        firsHorizontalLineDrawn = new boolean[]{hasBeenDrawn[0], hasBeenDrawn[1], hasBeenDrawn[2]};
        secondHorizontalLineDrawn = new boolean[]{hasBeenDrawn[3], hasBeenDrawn[4], hasBeenDrawn[5]};
        thirdHorizontalLineDrawn = new boolean[]{hasBeenDrawn[6], hasBeenDrawn[7], hasBeenDrawn[8]};

        lossAverted = new boolean[50];

        whoHasDrawn = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};

    }


    public void cpuMove() {


        if (!cpuPlayed) {


            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            verifyIfAboutToLoseDiagonally();

            if (!aboutToLoseDiagonally) {

                verifyIfAboutToLoseVertically();

                if(!aboutToLoseVertically) {

                    verifyIfAboutToLoseHorizontally();

                }

            }

            if (!willSkipATurn) {

                int randomizer = (int) Math.ceil(Math.random() * 9);
                System.out.println(randomizer);

                switch (randomizer) {

                    case 1:

                        if (!hasBeenDrawn[0]) {
                            playOnFirstRow(0, 0, 0, 0);
                        } else {
                            cpuMove();
                        }
                        break;
                    case 2:
                        if (!hasBeenDrawn[1]) {
                            playOnFirstRow(0, 1, 1, 1);
                        } else {
                            cpuMove();
                        }
                        break;
                    case 3:
                        if (!hasBeenDrawn[2]) {
                            playOnFirstRow(0, 2, 2, 2);
                        } else {
                            cpuMove();
                        }
                        break;
                    case 4:
                        if (!hasBeenDrawn[3]) {
                            playOnSecondRow(1, 0, 0, 3);
                        } else {
                            cpuMove();
                        }
                        break;
                    case 5:
                        if (!hasBeenDrawn[4]) {
                            playOnSecondRow(1, 1, 1, 4);
                        } else {
                            cpuMove();
                        }
                        break;
                    case 6:
                        if (!hasBeenDrawn[5]) {
                            playOnSecondRow(1, 2, 2, 5);
                        } else {
                            cpuMove();
                        }
                        break;
                    case 7:
                        if (!hasBeenDrawn[6]) {
                            playOnThirdRow(2, 0, 0, 6);
                        } else {
                            cpuMove();
                        }
                        break;
                    case 8:
                        if (!hasBeenDrawn[7]) {
                            playOnThirdRow(2, 1, 1, 7);
                        } else {
                            cpuMove();
                        }
                        break;
                    case 9:
                        if (!hasBeenDrawn[8]) {
                            playOnThirdRow(2, 2, 2, 8);
                        } else {
                            cpuMove();
                        }
                        break;
                }
            }
        }
    }



    public void verifyIfAboutToLoseHorizontally() {

        willSkipATurn = false;
        abouToLoseHorizontally = false;

        firstHorizontalLine = new int[]{whoHasDrawn[0], whoHasDrawn[1], whoHasDrawn[2]};
        secondHorizontalLine = new int[]{whoHasDrawn[3], whoHasDrawn[4], whoHasDrawn[5]};
        thirdHorizontalLine = new int[]{whoHasDrawn[6], whoHasDrawn[7], whoHasDrawn[8]};

        int length = firstHorizontalLine.length;

        int sumOfFirstHorizontal = 0;
        boolean avoidedLossOnFirstLineHorizontal = false;

        int sumOfSecondHorizontal = 0;
        boolean avoidedLossOnSecondLineHorizontal = false;

        int sumOfThirdHorizontal = 0;
        boolean avoidedLossOnThirdLineHorizontal = false;

        for (int i = 0; i < length; i++) {

            sumOfFirstHorizontal = sumOfFirstHorizontal + firstHorizontalLine[i];
            sumOfSecondHorizontal = sumOfSecondHorizontal + secondHorizontalLine[i];
            sumOfThirdHorizontal =  sumOfThirdHorizontal + thirdHorizontalLine[i];

        }


        if (sumOfFirstHorizontal == 8 && !avoidedLossOnFirstLineHorizontal) {


            for (int i = 0; i < firsHorizontalLineDrawn.length; i++) {

                if (!firsHorizontalLineDrawn[i] && !hasBeenDrawn[i]) {

                    drawXOn(i);
                    avoidedLossOnFirstLineHorizontal = true;
                    willSkipATurn = true;
                    abouToLoseHorizontally = true;

                }
            }
        }

        if (sumOfSecondHorizontal == 8 && !avoidedLossOnSecondLineHorizontal) {

            for (int i = 0; i < secondHorizontalLineDrawn.length; i++) {

                if (!secondHorizontalLineDrawn[i] && !hasBeenDrawn[i+3]) {

                    drawXOn(i+3);
                    avoidedLossOnSecondLineHorizontal = true;
                    willSkipATurn = true;
                    abouToLoseHorizontally = true;

                }
            }
        }

        if (sumOfThirdHorizontal == 8 && !avoidedLossOnThirdLineHorizontal) {

            for (int i = 0; i < thirdHorizontalLineDrawn.length; i++) {

                if (!thirdHorizontalLineDrawn[i] && !hasBeenDrawn[i+6]) {

                    drawXOn(i+6);
                    avoidedLossOnThirdLineHorizontal = true;
                    willSkipATurn = true;
                    abouToLoseHorizontally = true;

                }
            }
        }
    }

    private void verifyIfAboutToLoseVertically() {

        willSkipATurn = false;
        aboutToLoseVertically = false;

        firstVerticalLine = new int[]{whoHasDrawn[0], whoHasDrawn[3], whoHasDrawn[6]};
        secondVerticalLine = new int[]{whoHasDrawn[1], whoHasDrawn[4], whoHasDrawn[7]};
        thirdVerticalLine = new int[]{whoHasDrawn[2], whoHasDrawn[5], whoHasDrawn[8]};

        firstVerticalLineDrawn = new boolean[]{hasBeenDrawn[0], hasBeenDrawn[3], hasBeenDrawn[6]};
        secondVerticalLineDrawn = new boolean[]{hasBeenDrawn[1], hasBeenDrawn[4], hasBeenDrawn[7]};
        thirdVerticalLineDrawn = new boolean[]{hasBeenDrawn[2], hasBeenDrawn[5], hasBeenDrawn[8]};

        int length = firstVerticalLine.length;

        int sumOfFirstLineVertical = 0;
        boolean avoidedLossOnFirstLineVertical = false;

        int sumOfSecondLineVertical = 0;
        boolean avoidedLossOnSecondLineVertical = false;

        int sumOfThirdLineVertical = 0;
        boolean avoidedLossOnThirdLineVertical = false;

        for (int i = 0; i < length; i++) {

            sumOfFirstLineVertical = sumOfFirstLineVertical + firstVerticalLine[i];
            sumOfSecondLineVertical = sumOfSecondLineVertical + secondVerticalLine[i];
            sumOfThirdLineVertical =  sumOfThirdLineVertical + thirdVerticalLine[i];

        }


        if (sumOfFirstLineVertical == 8 && !avoidedLossOnFirstLineVertical) {

            for (int i = 0; i < firstVerticalLine.length; i++) {

                if (!firstVerticalLineDrawn[i]) {

                    drawXOn(i * 3);
                    avoidedLossOnFirstLineVertical = true;
                    willSkipATurn = true;
                    aboutToLoseVertically = true;

                }
            }
        }

        if (sumOfSecondLineVertical == 8 && !avoidedLossOnSecondLineVertical) {

            for (int i = 0; i < secondVerticalLine.length; i++) {

                if (!secondVerticalLineDrawn[i]) {

                    drawXOn(i * 3 + 1);
                    avoidedLossOnSecondLineVertical = true;
                    willSkipATurn = true;
                    aboutToLoseVertically = true;

                }
            }
        }

        if (sumOfThirdLineVertical == 8 && !avoidedLossOnThirdLineVertical)

            for (int i = 0; i < thirdVerticalLine.length; i++) {

                if (!thirdVerticalLineDrawn[i]) {

                    drawXOn(i * 3 + 2);
                    avoidedLossOnThirdLineVertical = true;
                    willSkipATurn = true;
                    aboutToLoseVertically = true;

                }
            }
        }


    private void verifyIfAboutToLoseDiagonally() {

        willSkipATurn = false;
        aboutToLoseDiagonally = false;

        diagonalLtoR = new int[]{whoHasDrawn[0], whoHasDrawn[4], whoHasDrawn[8]};
        diagonalRtoL = new int[]{whoHasDrawn[2], whoHasDrawn[4], whoHasDrawn[6]};

        lToRDiagonalDrawn = new boolean[]{hasBeenDrawn[0], hasBeenDrawn[4], hasBeenDrawn[8]};
        rToLDiagonalDrawn = new boolean[]{hasBeenDrawn[2], hasBeenDrawn[4], hasBeenDrawn[6]};

        int length = diagonalLtoR.length;

        int sumOfRtoL = 0;
        int sumOfLtoR = 0;

        boolean avoidedLossLtoR = false;
        boolean avoidedLossRtoL = false;

        for (int i = 0; i < length; i++) {

            sumOfLtoR = sumOfLtoR + diagonalLtoR[i];
            sumOfRtoL = sumOfRtoL + diagonalRtoL[i];

        }


        if (sumOfLtoR == 8 && !avoidedLossLtoR) {

            for (int i = 0; i < lToRDiagonalDrawn.length; i++) {

                if (!lToRDiagonalDrawn[i]) {

                    drawXOn(i*4);
                    avoidedLossLtoR = true;
                    willSkipATurn = true;
                    aboutToLoseDiagonally = true;

                }

            }

        }

        if (sumOfRtoL == 8 && !avoidedLossRtoL) {

            for (int i = 0; i < rToLDiagonalDrawn.length; i++) {

                if (!rToLDiagonalDrawn[i]) {

                    drawXOn(i*2+2);
                    avoidedLossRtoL = true;
                    willSkipATurn = true;
                    aboutToLoseDiagonally = true;

                }

            }

        }



    }



    public void playOnFirstRow(int i, int j, int k, int l) {
        xs.configureFirstLinePositions(board.getSpecificRectangle(i, j), k);
        xs.drawX();
        cpuPlayed = true;
        hasBeenDrawn[l] = true;
        whoHasDrawn[l] = 1;

    }

    public void playOnSecondRow(int i, int j, int k, int l) {
        xs.configureSecondLinePositions(board.getSpecificRectangle(i, j), k);
        xs.drawX();
        cpuPlayed = true;
        hasBeenDrawn[l] = true;
        whoHasDrawn[l] = 1;

    }

    public void playOnThirdRow(int i, int j, int k, int l) {
        xs.configuredThirdLinePositions(board.getSpecificRectangle(i, j), k);
        xs.drawX();
        cpuPlayed = true;
        hasBeenDrawn[l] = true;
        whoHasDrawn[l] = 1;

    }

    public void drawXOn(int i) {

        switch(i) {

            case 0 : playOnFirstRow(0, 0, 0, 0);
            break;
            case 1 : playOnFirstRow(0, 1, 1, 1);
            break;
            case 2 : playOnFirstRow(0, 2, 2, 2);
            break;
            case 3 : playOnSecondRow(1, 0, 0, 3);
            break;
            case 4 : playOnSecondRow(1, 1, 1, 4);
            break;
            case 5 : playOnSecondRow(1, 2, 2, 5);
            break;
            case 6 :  playOnThirdRow(2, 0, 0, 6);
            break;
            case 7 : playOnThirdRow(2, 1, 1, 7);
            break;
            case 8 : playOnThirdRow(2, 2, 2, 8);
            break;

        }


    }



    public boolean getCpuHasPlayed() {
        return this.cpuPlayed;
    }

    public void setCpuPlayed(boolean cpuPlayed) {
        this.cpuPlayed = cpuPlayed;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public boolean[] getHasBeenDrawn() {
        return hasBeenDrawn;
    }

    public void setHasBeenDrawn(boolean bool, int arrayLocation) {
        hasBeenDrawn[arrayLocation] = bool;
    }

    public void setWhoHasDrawn(int circle, int arrayLocation) {
        whoHasDrawn[arrayLocation] = circle;
    }

}
