import org.academiadecodigo.simplegraphics.graphics.Ellipse;

public class Os {

    private Board board;
    private Ellipse ellipse;


    public Os (Board board) {

        this.board = board;

    }

    public void configureFirstLinePositions(int row, int i) {

        switch (row) {

            case 1 :
                ellipse = new Ellipse(Board.PADDING + 25 + (board.getCellsize() * i), Board.PADDING + 25, board.getCellsize() - 50, board.getCellsize() - 50);
                break;
            case 2 :
                ellipse = new Ellipse(Board.PADDING + 25 + (board.getCellsize() * i), Board.PADDING + 25 + board.getCellsize(), board.getCellsize() - 50, board.getCellsize() - 50);
                break;
            case 3 :
                ellipse = new Ellipse(Board.PADDING + 25 + (board.getCellsize() * i), Board.PADDING + 25 + (board.getCellsize()*2), board.getCellsize() - 50, board.getCellsize() - 50);
                break;
        }


    }

   /* public void configureSecondLinePositions(int i) {

        ellipse = new Ellipse()

    }*/

    public void drawO() {
        ellipse.draw();
    }

}
